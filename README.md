**After cloning the project, run the following command:**

```
$ cordova platform add android
$ cordova plugin add cordova-plugin-camera
```

**Then you can use the following command to run the program**

```
$ phonegap serve
```